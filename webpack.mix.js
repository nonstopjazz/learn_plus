const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// CSS
mix.styles('resources/css/', 'public/css/app.css');

// JS
mix.scripts('resources/js/', 'public/js/app.js');

// fonts
mix.copy('resources/fonts/', 'public/fonts/');

// Images
mix.copy('resources/images/', 'public/images/');

// data
mix.copy('resources/data/', 'public/data');

// vendor
mix.copy('resources/vendor/', 'public/vendor');

